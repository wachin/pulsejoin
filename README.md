# PulseJoin

GUI для создания виртуального микрофона PulseAudio, с которого можно записывать звук одновременно настоящего микрофона и тот, что выводится на динамики.

![](img/img1.png)

![](img/img3.png) 

![](img/img4.png) 

## Пример использования

![](img/img2.png)

## Установка 

### Установка на Ubuntu

```
sudo add-apt-repository ppa:mikhailnov/pulsejoin -y
sudo apt update
sudo apt install pulsejoin --install-recommends
```

Для работы [рекомендуется](https://github.com/wwmm/pulseeffects/issues/99 "PulseEffects Issue #99"), но [не обязателен](https://gitlab.com/mikhailnov/pulsejoin/blob/master/pulsejoin.sh#L37) PulseAudio >= 12. Если у вас Ubuntu 18.04, то в ней PulseAudio 11, а для установки PulseAudio 12 на Ubuntu 18.04 сделайте:

```
sudo add-apt-repository ppa:mikhailnov/pulseeffects -y
sudo apt dist-upgrade
```

### Установка на Debian

Добавим репозиторий от Ubuntu с приоритетом 1 (про приоритеты см. в `man apt_preferences`) и установим пакет из него. Пакет `pulsejoin` не бинарный и поэтому полностью совместим с Debian/Astra/Deepin.

```
echo "deb http://ppa.launchpad.net/mikhailnov/pulsejoin/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/mikhailnov-ubuntu-pulsejoin-bionic.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys FE3AE55CF74041EAA3F0AD10D5B19A73A8ECB754 
echo -e "Package: * \nPin: release o=LP-PPA-mikhailnov-pulsejoin \nPin-Priority: 1" | sudo tee /etc/preferences.d/mikhailnov-ubuntu-pulsejoin-ppa
sudo apt update
sudo apt install pulsejoin
```

### Установка на ALT Linux

Пакет *.rpm для ALT Linux можно скачать в [тегах](https://gitlab.com/mikhailnov/pulsejoin/tags "Tags"). Для установки в папке со скачанным пакетом выполните:  
`su -c "apt-get install ./pulsejoin*.rpm"`

### Установка на ROSA Fresh

PulseJoin естьв  репозитории ROSA Fresh и обновляется там автором PulseJoin. Установка:  
`sudo urpmi pulsejoin`

### Установка на другие дистрибутивы

Для работы нужны: **yad**, bash, pulseaudio.

```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
sudo make install
```

## Сборка пакетов

### Сборка Deb-пакета

```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
dpkg-buildpackage; debian/rules clean
sudo apt install ../pulsejoin*.deb
```

### Сборка RPM-пакета на ALT Linux

```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
rpmbb # должен быть установлен пакет etersoft-build-utils
su -c "apt-get install $HOME/RPM/RPMS/noarch/pulsejoin-*.rpm"
```

